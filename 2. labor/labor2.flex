%option noyywrap
%option yylineno
%{
#include <iostream>
#include "labor2.tab.h"
using namespace std;
int position = 0;
int poz[] = {1, 1, 0};
%}
%%
"#" {
    poz[1]++;
	poz[2] = yyleng;
    return yytext[0];
}
"$" {
    poz[1]++;
	poz[2] = yyleng;
    return yytext[0];
}
\`[^\`]*\` {
    poz[1] += yyleng;
	poz[2] = yyleng;
    }
\"[^\"]*\" {
    poz[1] += yyleng;
	poz[2] = yyleng;
    return STRING_VALUE;
}
"attaca" {
    poz[1] += yyleng;
	poz[2] = yyleng;
    return THEN;
}
"da capo al fine" {
    poz[1] += yyleng;
	poz[2] = yyleng;
    return GO_TO_START;
}
"fine" {
    poz[1] += yyleng;
	poz[2] = yyleng;
    return FINE;
}
"vide" {
    poz[1] += yyleng;
	poz[2] = yyleng;
    return OUT;
}
"prima vista" {
    poz[1] += yyleng;
	poz[2] = yyleng;
    return IN;
}

"corona" {
    poz[1] += yyleng;
	poz[2] = yyleng;
    return END_IF;
}
"|_" {
    poz[1] += yyleng;
	poz[2] = yyleng;
    return OPEN_PARENTHESIS;
}
"_|" {
    poz[1] += yyleng;
	poz[2] = yyleng;
    return CLOSE_PARENTHESIS;
}
"prima volta" {
    poz[1] += yyleng;
	poz[2] = yyleng;
    return IF;
}
"secunda volta" {
    poz[1] += yyleng;
	poz[2] = yyleng;
    return ELSE;
}
"==" {
    poz[1] += yyleng;
	poz[2] = yyleng;
    return EQUALS;
}
"~=" {
    poz[1] += yyleng;
	poz[2] = yyleng;
    return NOT_EQUALS;
}
"~" {
    poz[1] += yyleng;
	poz[2] = yyleng;
    return NOT;
}
"legato" {
    poz[1] += yyleng;
	poz[2] = yyleng;
    return AND;
}
"=" {
    poz[1] += yyleng;
	poz[2] = yyleng;
    return GIVE_VALUE;
}
"+"|"-"|"/"|"*" {
    poz[1]++;
	poz[2] = 1;
    return yytext[0];
}
".|" {
    poz[1] += yyleng;
	poz[2] = yyleng;
    return INTEGER;
}
".|.|" {
    poz[1] += yyleng;
	poz[2] = yyleng;
    return STRING;
}
".|`" {
    poz[1] += yyleng;
	poz[2] = yyleng;
    return FLOAT;
}
[a-z]+[a-zA-Z0-9_]* {
    poz[1] += yyleng;
	poz[2] = yyleng;
    return VARIABLE;
}
\|\|: {
    poz[1] += yyleng;
	poz[2] = yyleng;
    return BEGIN_LOOP;
}
:\|\| {
    poz[1] += yyleng;
	poz[2] = yyleng;
    return END_LOOP;
}
[\+-]?[0-9]+\.[0-9]+ {
    poz[1] += yyleng;
	poz[2] = yyleng;
    return FLOAT_VALUE;
}
[\+-]?[0-9]+ {
    poz[1] += yyleng;
	poz[2] = yyleng;
    return INTEGER_VALUE;
}
" "|\t|\r {
    poz[1] += yyleng;
	poz[2] = yyleng;
}
\n {
    poz[1] = 1;
	poz[0]++;
	poz[2] = 1;
}
. {return yytext[0];}
%%
