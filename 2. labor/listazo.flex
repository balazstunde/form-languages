%option noyywrap
%option yylineno
%{
#include <iostream>
using namespace std;
int position = 0;
%}
%%
"#"         {cout<<"[sor: "<<yylineno<<" oszlop: "<<position<<" hossz: "<<yyleng<<"] "<<yytext<<"valtozo++"<<endl; position = position + yyleng;}
"b"         {cout<<"[sor: "<<yylineno<<" oszlop: "<<position<<" hossz: "<<yyleng<<"] "<<yytext<<"- valtozo--"<<endl; position = position + yyleng;}
\`[^\`]*\` {cout<<"[sor: "<<yylineno<<" oszlop: "<<position<<" hossz: "<<yyleng<<"] "<<yytext<<"- Komment"<<endl; position = position + yyleng;}
\"[^\"]*\" {cout<<"[sor: "<<yylineno<<" oszlop: "<<position<<" hossz: "<<yyleng<<"] "<<yytext<<"- String"<<endl; position = position + yyleng;}
"disztonal" {cout<<"[sor: "<<yylineno<<" oszlop: "<<position<<" hossz: "<<yyleng<<"] "<<yytext<<"- Hiba"<<endl; position = position + yyleng;}
"attaca" {cout<<"[sor: "<<yylineno<<" oszlop: "<<position<<" hossz: "<<yyleng<<"] "<<yytext<<"- Then"<<endl; position = position + yyleng;}
"da capo al fine" {cout<<"[sor: "<<yylineno<<" oszlop: "<<position<<" hossz: "<<yyleng<<"] "<<yytext<<"- Ugras"<<endl; position = position + yyleng;}
"fine" {cout<<"[sor: "<<yylineno<<" oszlop: "<<position<<" hossz: "<<yyleng<<"] "<<yytext<<"- Vege"<<endl; position = position + yyleng;}
"vide" {cout<<"[sor: "<<yylineno<<" oszlop: "<<position<<" hossz: "<<yyleng<<"] "<<yytext<<"- Kiir"<<endl; position = position + yyleng;}
"prima vista" {cout<<"[sor: "<<yylineno<<" oszlop: "<<position<<" hossz: "<<yyleng<<"] "<<yytext<<"- Beolvas"<<endl; position = position + yyleng;}
"coda" {cout<<"[sor: "<<yylineno<<" oszlop: "<<position<<" hossz: "<<yyleng<<"] "<<yytext<<"- Finally"<<endl; position = position + yyleng;}
"corona" {cout<<"[sor: "<<yylineno<<" oszlop: "<<position<<" hossz: "<<yyleng<<"] "<<yytext<<"- End"<<endl; position = position + yyleng;}
"|_" {cout<<"[sor: "<<yylineno<<" oszlop: "<<position<<" hossz: "<<yyleng<<"] "<<yytext<<"- Nyito feltetel"<<endl; position = position + yyleng;}
"_|" {cout<<"[sor: "<<yylineno<<" oszlop: "<<position<<" hossz: "<<yyleng<<"] "<<yytext<<"- Zaro feltetel"<<endl; position = position + yyleng;}
"prima volta" {cout<<"[sor: "<<yylineno<<" oszlop: "<<position<<" hossz: "<<yyleng<<"] "<<yytext<<"- If"<<endl; position = position + yyleng;}
"secunda volta" {cout<<"[sor: "<<yylineno<<" oszlop: "<<position<<" hossz: "<<yyleng<<"] "<<yytext<<"- Else"<<endl; position = position + yyleng;}
"=="|"~="|"~"|"legato" {cout<<"[sor: "<<yylineno<<" oszlop: "<<position<<" hossz: "<<yyleng<<"] "<<yytext<<"- Logikai operatorok"<<endl; position = position + yyleng;}
"="|"+"|"-"|"/"|"*" {cout<<"[sor: "<<yylineno<<" oszlop: "<<position<<" hossz: "<<yyleng<<"] "<<yytext<<"- Aritmetikai operatorok"<<endl; position = position + yyleng;}
(\.\|\`_?[a-zA-Z][a-zA-Z]*[0-9]*) {cout<<"[sor: "<<yylineno<<" oszlop: "<<position<<" hossz: "<<yyleng<<"] "<<yytext<<"- Valtozo valos"<<endl; position = position + yyleng;}
(\.\|\.\|_?[a-zA-Z][a-zA-Z]*[0-9]*) {cout<<"[sor: "<<yylineno<<" oszlop: "<<position<<" hossz: "<<yyleng<<"] "<<yytext<<"- Valtozo tomb"<<endl; position = position + yyleng;}
(\.\|_?[a-zA-Z][a-zA-Z]*[0-9]*) {cout<<"[sor: "<<yylineno<<" oszlop: "<<position<<" hossz: "<<yyleng<<"] "<<yytext<<"- Valtozo egesz"<<endl; position = position + yyleng;}
\|\|: {cout<<"[sor: "<<yylineno<<" oszlop: "<<position<<" hossz: "<<yyleng<<"] "<<yytext<<"- For kezdete"<<endl; position = position + yyleng;}
[\+-]?[0-9]+\.[0-9]+ {cout<<"[sor: "<<yylineno<<" oszlop: "<<position<<" hossz: "<<yyleng<<"] "<<yytext<<"- Valos literal"<<endl; position = position + yyleng;}
[\+-]?[0-9]+ {cout<<"[sor: "<<yylineno<<" oszlop: "<<position<<" hossz: "<<yyleng<<"] "<<yytext<<"- Egesz literal"<<endl; position = position + yyleng;}
:\|\| {cout<<"[sor: "<<yylineno<<" oszlop: "<<position<<" hossz: "<<yyleng<<"] "<<yytext<<"- For vege"<<endl; position = position + yyleng;}
\n {position = 0;}
. {position = position + 1;}
%%
int main(){
    yyin = fopen("be.txt","r");
    yylex();
    fclose(yyin);
}