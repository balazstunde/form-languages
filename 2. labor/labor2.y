%{
#include <stdio.h>
#include <stdlib.h>    
#include <string.h>
#include <iostream>

using namespace std;
extern int yylex();
int yyerror(const char*);
extern int yyparse();
extern int poz[];
extern "C" FILE *yyin;
%}

%error-verbose

%token GIVE_VALUE
%token IN OUT
%token VARIABLE FLOAT_VALUE INTEGER_VALUE STRING_VALUE
%token EQUALS NOT_EQUALS
%token AND NOT
%token IF THEN ELSE END_IF
%token BEGIN_LOOP END_LOOP
%token FINE GO_TO_START
%token OPEN_PARENTHESIS CLOSE_PARENTHESIS



%left INTEGER FLOAT STRING GIVE_VALUE IN OUT VARIABLE FLOAT_VALUE INTEGER_VALUE STRING_VALUE EQUALS NOT_EQUALS IF THEN ELSE END_IF BEGIN_LOOP END_LOOP FINE GO_TO_START
%left AND 
%left NOT
%left '+' '-' '$' '#' 
%left '*' '/'
// flex:
// yylval.ival = 43
// yylval.fval = 7.5
// return yyval.ival;
// bison:
// $1 + $2 helyett
// $<ival>1 + ...
%union {
    int ival;
    float fval;
    //char * vname; amikor majd a valtozot is at kell adni?
}

%start s

%%

s: statement
 | s statement
;

expr: expression
    | sub
    | add
;

expression: FLOAT_VALUE
    | INTEGER_VALUE
    | VARIABLE
    | expr '+' expr 
    | expr '-' expr 
    | expr '*' expr 
    | expr '/' expr 
    | OPEN_PARENTHESIS expr CLOSE_PARENTHESIS
;

sub: VARIABLE '$';

add: VARIABLE '#';

condition_expr:
   | NOT condition 
   | condition AND condition 
;

condition: expr EQUALS expr 
	| expr NOT_EQUALS expr 
    
;

statement: VARIABLE GIVE_VALUE expr
    | error statement {yyerrok;}
    | sub
    | add
	| BEGIN_LOOP OPEN_PARENTHESIS condition CLOSE_PARENTHESIS s END_LOOP 
	| IF OPEN_PARENTHESIS condition CLOSE_PARENTHESIS THEN s END_IF
	| IF OPEN_PARENTHESIS condition CLOSE_PARENTHESIS THEN s ELSE s END_IF 
	| BEGIN_LOOP OPEN_PARENTHESIS condition_expr CLOSE_PARENTHESIS s END_LOOP
	| IF OPEN_PARENTHESIS condition_expr CLOSE_PARENTHESIS THEN s END_IF 
	| IF OPEN_PARENTHESIS condition_expr CLOSE_PARENTHESIS THEN s ELSE s END_IF
    | FLOAT VARIABLE GIVE_VALUE expr
    | INTEGER VARIABLE GIVE_VALUE expr
    | STRING VARIABLE GIVE_VALUE STRING_VALUE
	| OUT expr 
	| IN VARIABLE
;

%%


int main(int argc, char* argv[]) {
    if(argc > 1){
        yyin = fopen(argv[1], "r");
    }
	if (yyparse()==0)
		printf("parsing complete.");
}

int yyerror(const char* s) {
    cout << "(" << poz[0] << "," << poz[1]-poz[2] << "): ";
    cout << s << endl;
} 