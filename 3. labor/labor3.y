%{
#include <stdio.h>
#include <stdlib.h>    
#include <string.h>
#include <string>
#include <iostream>
#include <map>
#include <vector>

using namespace std;
extern int yylex();

int yyerror(const char*);
void yyerror_variables_type_array(string, string);
void add_global_variable_to_symbol_table(string, string );
void add_to_symbol_table(string, string);
void delete_level();
void add_level();
string check_variables_type(string);
bool check_variable_in_symbol_table(string);
void print_table();
void yyerror_variable(const char*);
void yyerror_variable_exists(const char*);
void yyerror_variables_type(const char*, const char*, const char*);

extern int yyparse();
extern int poz[];
extern "C" FILE *yyin;

int level=-1;
int col, row;
vector<map<string, string>> levels;
vector<map<string, string>> values;
%}

%error-verbose

%token INTEGER FLOAT STRING ARRAY
%token POP PUSH
%token GLOBAL
%token GIVE_VALUE
%token IN OUT
%token VARIABLE FLOAT_VALUE INTEGER_VALUE STRING_VALUE
%token EQUALS NOT_EQUALS
%token AND NOT
%token IF THEN ELSE END_IF
%token BEGIN_LOOP END_LOOP
%token FINE GO_TO_START
%token OPEN_PARENTHESIS CLOSE_PARENTHESIS

%left ARRAY INTEGER FLOAT STRING GIVE_VALUE IN OUT VARIABLE FLOAT_VALUE INTEGER_VALUE STRING_VALUE EQUALS NOT_EQUALS IF THEN ELSE END_IF BEGIN_LOOP END_LOOP FINE GO_TO_START GLOBAL
%left AND 
%left NOT
%left '+' '-' '$' '#' 
%left '*' '/'
// flex:
// yylval.ival = 43
// yylval.fval = 7.5
// return yyval.ival;
// bison:
// $1 + $2 helyett
// $<ival>1 + ...
%union {
    //int ival;
    //float fval;
	char *variable_name;
}

%start s

%%

s: statement
 | s statement
;

expr: expression
    | sub
    | add
;

expression: INTEGER_VALUE
    | FLOAT_VALUE
    | var {
        if(!check_variable_in_symbol_table($<variable_name>1)){
            yyerror_variable($<variable_name>1);
        }else{
            if(check_variables_type($<variable_name>1) == "string"){
                yyerror_variables_type($<variable_name>1, "number", "string");
            }else{
                if(check_variables_type($<variable_name>1) == "intarray"){
                    yyerror_variables_type($<variable_name>1, "number", "array");
                }else{
                    if(check_variables_type($<variable_name>1) == "floatarray"){
                        yyerror_variables_type($<variable_name>1, "number", "array");
                    }
                }
            }
        }
    }
    | expression '+' expression
    | expression '-' expression
    | expression '*' expression
    | expression '/' expression
    | OPEN_PARENTHESIS expression CLOSE_PARENTHESIS
;



sub: var '$' {
        if(!check_variable_in_symbol_table($<variable_name>1)){
            yyerror_variable($<variable_name>1);
        }
    }
;

add: var '#' {
        if(!check_variable_in_symbol_table($<variable_name>1)){
            yyerror_variable($<variable_name>1);
        }
    }
;

condition_expr:
   | NOT condition 
   | condition AND condition 
;

condition: expr EQUALS expr 
	| expr NOT_EQUALS expr 
    
;

if_block: IF {add_level();}
;

else_block: ELSE {delete_level(); add_level();}
;

loop_block: BEGIN_LOOP {add_level();}
;

pop: var POP {
    if(!check_variable_in_symbol_table($<variable_name>1)){
            yyerror_variable($<variable_name>1);
    }else{
        if(!check_variable_in_symbol_table($<variable_name>1)){
            yyerror_variable($<variable_name>1);
        }else{
            if(check_variables_type($<variable_name>1) == "string"){
                yyerror_variables_type($<variable_name>1, "array", "string");
            }else{
                if(check_variables_type($<variable_name>1) == "int"){
                    yyerror_variables_type($<variable_name>1, "array", "int");
                }else{
                    if(check_variables_type($<variable_name>1) == "float"){
                        yyerror_variables_type($<variable_name>1, "array", "float");
                    }
                }
            }
        }
        //switch(check_variables_type($<variable_name>1) ){
        //    case "string": yyerror_variables_type($<variable_name>1, "array", "string"); break;
        //    case "int": yyerror_variables_type($<variable_name>1, "array", "int"); break;
        //    case "float": yyerror_variables_type($<variable_name>1, "array", "float"); break;
        //}
    }
}
;

push: var PUSH var {
    if(!check_variable_in_symbol_table($<variable_name>1)){
        yyerror_variable($<variable_name>1);
    }else{
        if(!check_variable_in_symbol_table($<variable_name>3)){
            yyerror_variable($<variable_name>3);
        }else{
            string first_type = check_variables_type($<variable_name>1);
            string second_type = check_variables_type($<variable_name>3);
            if(!((first_type=="intarray" && second_type=="int") || (first_type=="floatarray" && second_type=="float"))){
                yyerror_variables_type_array(first_type, second_type);
            }
        }
        
    }
}
;

var: VARIABLE {row =  poz[0]; col = poz[1]-poz[2]-strlen($<variable_name>1);}
;

statement: var GIVE_VALUE expr {
        if(!check_variable_in_symbol_table($<variable_name>1)){
            yyerror_variable($<variable_name>1);
        }
    }
    | error statement {yyerrok;}
    | sub
    | add
	| loop_block OPEN_PARENTHESIS condition CLOSE_PARENTHESIS s END_LOOP {delete_level();}
	| if_block OPEN_PARENTHESIS condition CLOSE_PARENTHESIS THEN s END_IF {delete_level(); }
	| if_block OPEN_PARENTHESIS condition CLOSE_PARENTHESIS THEN s else_block s END_IF {delete_level();}
	| loop_block OPEN_PARENTHESIS condition_expr CLOSE_PARENTHESIS s END_LOOP {delete_level();}
	| if_block OPEN_PARENTHESIS condition_expr CLOSE_PARENTHESIS THEN s END_IF {delete_level();}
	| if_block OPEN_PARENTHESIS condition_expr CLOSE_PARENTHESIS THEN s else_block s END_IF {delete_level();}
    | pop
    | push
    | INTEGER ARRAY VARIABLE {
        add_to_symbol_table($<variable_name>3, "intarray");
    }
    | FLOAT ARRAY VARIABLE {
        add_to_symbol_table($<variable_name>3, "floatarray");
    }
    | FLOAT VARIABLE GIVE_VALUE expr {
        add_to_symbol_table($<variable_name>2, "float");
    }
    | GLOBAL FLOAT VARIABLE GIVE_VALUE expr {
        add_global_variable_to_symbol_table($<variable_name>3, "float");
    }
    | INTEGER VARIABLE GIVE_VALUE expr {
        add_to_symbol_table($<variable_name>2, "int");
    }
    | GLOBAL INTEGER VARIABLE GIVE_VALUE expr {
        add_global_variable_to_symbol_table($<variable_name>3, "int");
    }
    | STRING VARIABLE GIVE_VALUE STRING_VALUE {
        add_to_symbol_table($<variable_name>2, "string");
    }
    | GLOBAL STRING VARIABLE GIVE_VALUE STRING_VALUE {
        add_global_variable_to_symbol_table($<variable_name>3, "string");
    }
	| OUT expr 
	| IN var {
        if(!check_variable_in_symbol_table($<variable_name>2)){
            if(!check_variable_in_symbol_table($<variable_name>2)){
                yyerror_variable($<variable_name>2);
            }
        }
    }
;

%%

void add_global_variable_to_symbol_table(string variable, string type){
    if(levels[0].find(variable) == levels[0].end()){
        levels[0].insert(pair<string,string>(variable,type));
        //local_symbol_table[variable] = type;
    }else{
        yyerror_variable_exists(variable.c_str());
    }
}

void add_to_symbol_table(string variable, string type){

    if(levels[0].find(variable) == levels[0].end()){
        if(levels[level].find(variable) == levels[level].end()){
            levels[level].insert(pair<string,string>(variable,type));
        //local_symbol_table[variable] = type;
        }else{
            yyerror_variable_exists(variable.c_str());
        }
    }else{
        yyerror_variable_exists(variable.c_str());
    }
    
}

void delete_level(){
    levels.pop_back();
    level--;
}

void add_level(){
    map<string, string> mymap;
    levels.push_back(mymap);
    level++;
}

void initialize_levels(){
    add_level();
    add_level();
}

string check_variables_type(string variable){
    int i = level;
    while (i >= 0){
        if(levels[i].find(variable) != levels[i].end()){
            return levels[i].find(variable)->second.c_str();
        }
        i--;
    }
}

bool check_variable_in_symbol_table(string variable){
    bool found = false;
    int i = level;
    while (!found && i >= 0){
        found = levels[i].count(variable) != 0;
        i--;
    }
    return found;	
	//cout<<"Variable "<<variable.c_str()<<" was not declared before in local table."; 
}

void print_table(){
    cout<<"List of variables:"<<endl;
    for(int i = 0; i < levels.size(); i++){
        map<string,string>::iterator iterator;
        for(iterator = levels[i].begin(); iterator != levels[i].end(); ++iterator){
            cout<<i<< ". level, symbol: " << iterator->first.c_str() <<" -> type: "<< iterator->second.c_str()<<endl;
        }
    }
}

int main(int argc, char* argv[]) {
    if(argc > 1){
        yyin = fopen(argv[1], "r");
    }
    initialize_levels();
	if (yyparse()==0)
		printf("parsing complete.");
}

void yyerror_variable(const char* s){
    cout << "Undeclared variable \"" << s << "\" at line: " << row << "." << endl;
}

void yyerror_variable_exists(const char* s){
    cout << "Variable \"" << s << "\" at line: " << row << " already exists." << endl;
}

void yyerror_variables_type(const char* s, const char* type1, const char* type2){
    cout << "Types \""<< type1 << "\" and \"" <<type2 <<"\" do not match at line: " << row << "."<<endl;
}

void yyerror_variables_type_array(string type1, string type2){
    cout << "Types \""<< type1 << "\" and \"" <<type2 <<"\" do not match at line: " << row << "." << endl;
}

int yyerror(const char* s) {
    cout << "(" << poz[0] << "," << poz[1]-poz[2] << "): ";
    cout << s << "." << endl;
} 

